import React, { useState } from 'react';
import axios from 'axios'

export default function LoginForm(props) {
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');

    function handleSubmit (e) {
        e.preventDefault();

        const url = '/api/login';
        axios.post(url, {
            email: email, password: password
        })
            .then((resp) => {
                console.log(resp.data);
                alert('OK. Token: ' + JSON.stringify(resp.data));
            })
            .catch(function (error) {
                console.error(error.response.data);
                alert('Not OK. Error: ' + JSON.stringify(error.response.data));
            });
    };

    return (
         <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor='email'>Email</label>
                 <input type='email' id='email' value={props.email} onChange={(e) => setEmail(e.target.value)}  />
            </div>
            <div>
                <label htmlFor='password'>Password</label>
                 <input type='password' id='password' value={props.password} onChange={(e) => setPassword(e.target.value)}/>
            </div>
            <button>Login</button>
        </form>
    );
}
